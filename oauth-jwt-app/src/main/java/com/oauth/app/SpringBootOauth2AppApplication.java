package com.oauth.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootOauth2AppApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootOauth2AppApplication.class, args);
	}
}
