package com.oauth.app.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class HomeController {

    @GetMapping({"/",""})
    @PreAuthorize("hasRole('USER')")
    public String index(){
        return "Some text";
    }

    @GetMapping("/greetings")
    @PreAuthorize("hasRole('ADMIN')")
    public String greetings(){
        return "Some text from method greetings()";
    }
}
